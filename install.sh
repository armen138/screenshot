pip install -r requirements.txt
sed -i "s|PWD|$PWD|" screenshot.desktop
mkdir ~/Screenshots
mkdir -p ~/.local/share/applications
cp screenshot.desktop ~/.local/share/applications/
echo "Ensure gvfs or gvfs-fuse (depending on distro) is installed"
if command -v pacman &> /dev/null
then
    sudo pacman -S gvfs grim
fi
if command -v apt &> /dev/null
then
    sudo apt install gvfs-fuse grim
fi
