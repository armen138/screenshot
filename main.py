import gi
import os
from datetime import datetime

# using this instead of PIL because it has wayland support
import pyscreenshot as ImageGrab

gi.require_version("Gtk", "3.0")
gi.require_version("Goa", "1.0")
from gi.repository import Gtk, GLib, GdkPixbuf, Goa, Gio

screen_width = 0
screen_height = 0

def screenshot(geometry, filename):
    image = ImageGrab.grab(bbox=(geometry.x, geometry.y, geometry.width, geometry.height))
    image.save(filename)
    win.on_after_screenshot()
    # run only once
    return False

class OnlineAccounts():
    def __init__(self):
        client = Goa.Client.new_sync(None)
        self.accounts = client.get_accounts()

    def list(self):
        account_list = []
        for account in self.accounts:
            if account.get_files():
                account_list.append({
                    "name": account.get_account().props.provider_name,
                    "icon": account.get_account().props.provider_icon.split(" ")[2]
                })
        return account_list

    def get_account_by_provider(self, provider_name):
        for account in self.accounts:
            name = account.get_account().props.provider_name
            if name == provider_name:
                return account
        return None

    def replace_content(self, filename, destination_uri):
        file = Gio.File.new_for_uri(destination_uri)
        with open(filename, "rb") as image_file:
            file.replace_contents(image_file.read(), None, False, Gio.FileCreateFlags.REPLACE_DESTINATION, None)

    def send_file(self, account_name, filename, destination):
        account = self.get_account_by_provider(account_name)
        name = account.get_account().props.provider_name
        if name == account_name:
            files = account.get_files()
            location = files.props.uri + "/" + destination
            try:
                self.replace_content(filename, location)
            except GLib.Error as e:
                if e.code == 1: # Not Found
                    path = Gio.File.new_for_uri(os.path.dirname(location))
                    success = path.make_directory(None)
                    print(success)
                    if success:
                        self.replace_content(filename, location)
                    else:
                        print("Failed to create required path on remote")
                elif e.code == 17: # Location is already mounted
                    pass
                elif e.code == 16: # Location is not mounted
                    mount_location = Gio.File.new_for_uri(files.props.uri)
                    operation = Gio.MountOperation()
                    operation.connect('ask-password', self.ask_password, account)
                    file_data = {
                        "filename": filename,
                        "destination": destination
                    }
                    mount_location.mount_enclosing_volume(Gio.MountMountFlags.NONE, operation, None, self.on_mounted, file_data)

    def ask_password(self, operation, message, default_user, default_domain, flags, account):
        pw_based = account.get_password_based()
        password = pw_based.call_get_password_sync("", None)
        operation.set_password(password)
        operation.reply(Gio.MountOperationResult.HANDLED)

    def on_mounted(self, file, task, file_data):
        file.mount_enclosing_volume_finish(task)
        self.replace_content(file_data["filename"], file.get_uri() + "/" + file_data["destination"])

class ScreenshotWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Screenshot")
        self.set_default_size(360, 720)
        self.set_position(Gtk.WindowPosition.CENTER_ALWAYS)
        self.accounts = OnlineAccounts()
        self.create_buttons()
        GLib.timeout_add(500, self.shoot)

    def alert(self, message_type, message):
        info_bar = Gtk.InfoBar()
        info_bar.set_message_type(message_type)
        label = Gtk.Label(label=message)
        info_bar.get_content_area().add(label)
        info_bar.set_show_close_button(True)
        self.grid.add(info_bar)
        info_bar.show_all()
        info_bar.connect("response", self.hide_info_bar)
        info_bar.set_revealed(True)

    def hide_info_bar(self, info_bar, response):
        info_bar.hide()

    def create_buttons(self):
        grid = Gtk.Grid()
        grid.set_orientation(Gtk.Orientation.VERTICAL)
        self.image = Gtk.Image(vexpand=True)
        grid.add(self.image)
        for account in self.accounts.list():
            upload_button = Gtk.Button(label="Send to " + account["name"])
            upload_button.online_account = account["name"]
            upload_button.connect("clicked", self.on_upload)
            icon = Gtk.Image.new_from_icon_name(account["icon"], Gtk.IconSize.BUTTON)
            upload_button.set_image(icon)
            upload_button.props.always_show_image = True
            upload_button.set_size_request(100, 100)
            grid.add(upload_button)
        cancel = Gtk.Button(label="Exit")
        cancel.set_hexpand(True)
        cancel.connect("clicked", self.on_exit)
        cancel.set_size_request(100, 100)
        grid.add(cancel)
        self.add(grid)
        self.grid = grid

    def on_upload(self, widget):
        self.accounts.send_file(widget.online_account, self.filename, self.filename)
        self.alert(Gtk.MessageType.INFO, "File uploaded to " + widget.online_account)
        widget.hide()

    def get_monitor_geometry(self, monitor):
        display = self.get_display()
        monitor = display.get_monitor(0)
        return monitor.get_geometry()

    def shoot(self):
        self.hide()
        now = datetime.now()
        self.filename = now.strftime("Screenshots/Screenshot_%Y_%m_%d_%H_%M_%S.png")
        GLib.timeout_add(500, screenshot, self.get_monitor_geometry(0), self.filename)

    def on_exit(self, widget):
        self.destroy()

    def on_after_screenshot(self):
        geometry = self.get_monitor_geometry(0)
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(self.filename, geometry.width * 0.6, geometry.height * 0.6, True)
        self.image.set_from_pixbuf(pixbuf)
        self.show()

win = ScreenshotWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
