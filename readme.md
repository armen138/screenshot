# Screenshot

A small python script to save a screenshot, and optionally send it to an online account.

> Note: online accounts are supported through Gnome Online Accounts (GOA), and must provide the Files service.
> As far as I can tell, that's just NextCloud, at the moment.

In order to mount the remote location, you may need to install an extra package.
On Arch, it is "gvfs", on debian-esque distros "gvfs-fuse".

The pyscreenshot module requires a local backend to take the actual screenshot, and one needs to be installed. "grim" is known to work well.

This script relies on Python 3.7+

The provided install shell script will copy the desktop file to .local/share/applications and install the required python modules.

This is primarily meant to run on mobile linux devices, but will work fine on traditional setups as well.